package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class Individual {
    private WebDriver chromeDriver;

    private static final String baseUrl = "https://rozetka.com.ua/";

    @BeforeClass(alwaysRun = true)
    public void setUp() {
        //Run driver
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        //set fullscreen
        chromeOptions.addArguments("--start-fullscreen");
        //setup wait for loading elements
        chromeOptions.setImplicitWaitTimeout(Duration.ofSeconds(15));
        this.chromeDriver = new ChromeDriver(chromeOptions);
    }

    @BeforeMethod
    public void preconditions(){
        //open main page
        chromeDriver.get(baseUrl);
    }

    //1 Завдання - Клік по елементу
    @Test
    public void testClickOnButton(){
        //The test is successful. Clicking the button - Catalog
        WebElement clickable = chromeDriver.findElement(By.id("fat-menu"));
        clickable.click();
    }

    //2 Завдання - Введення даних у поле та перевірка поля на наявність даних
    @Test
    public void testEnterDataFieldAndCheckAndSearch(){
        //The test is successful.
        WebElement searchField = chromeDriver.findElement(By.name("search"));
        //verification
        Assert.assertNotNull(searchField);
        //String inputValue
        String inputValue = "Таблетки для посудомоечных машин";
        searchField.sendKeys(inputValue);
        //different params of searchField
        System.out.println( String.format("Name attribute: %s", searchField.getAttribute("name")) +
                String.format("\nID attribute: %s", searchField.getAttribute("id")) +
                String.format("\nType attribute: %s", searchField.getAttribute("type")) +
                String.format("\nValue attribute: %s", searchField.getAttribute("value")) +
                String.format("\nPosition: (%d;%d)", searchField.getLocation().x, searchField.getLocation().y) +
                String.format("\nSize: %dx%d", searchField.getSize().height, searchField.getSize().width));
        //verification text
        Assert.assertEquals(searchField.getAttribute("value"),inputValue);
        //click enter
        searchField.sendKeys(Keys.ENTER);
    }

    //3 Завдання - Знаходження елементу за допомогою не прямого XPath (використовуючи функції або унікальні ідентифікатори)
    @Test
    public void testClickOnSignIn(){
        //The test is successful.
        //Find on the "Log in to my personal account" page. The button is located after the words "Welcome" in the left pane.
        //find element by xpath
        WebElement signInButton = chromeDriver.findElement(By.xpath("//button[contains(text(),'Увійдіть')]"));
        //verification
        Assert.assertNotNull(signInButton);
        signInButton.click();
    }

    //4 Завдання - Перевірку будь-якої умови
    @Test
    public void testCheckAnyCondition(){
        //The test is failed.
        //find element by class name
        WebElement nextButton = chromeDriver.findElement(By.xpath("/html/body/app-root/div/div/rz-main-page/div/main/rz-main-page-content/div/rz-top-slider/app-slider/div/button[2]"));

        WebElement previousButton = chromeDriver.findElement(By.xpath("/html/body/app-root/div/div/rz-main-page/div/main/rz-main-page-content/div/rz-top-slider/app-slider/div/button[1]"));

        for (int i = 0; i < 11; i++) {
            //change count of interations just for run
            if (nextButton.getAttribute("class").contains("disabled")) {
                previousButton.click();
                Assert.assertTrue(previousButton.getAttribute("class").contains("disabled"));
                Assert.assertFalse(nextButton.getAttribute("class").contains("disabled"));
            } else {
                nextButton.click();
                Assert.assertTrue(nextButton.getAttribute("class").contains("disabled"));
                Assert.assertFalse(previousButton.getAttribute("class").contains("disabled"));
            }
        }
    }

    @AfterClass(alwaysRun = true)
    public void tearDown(){
        chromeDriver.quit();
    }
}