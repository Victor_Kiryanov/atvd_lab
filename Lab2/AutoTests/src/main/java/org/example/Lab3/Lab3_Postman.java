package org.example.Lab3;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class Lab3_Postman {
    private static final String baseUrl = "https://9192b4a7-05ea-460c-8020-730d7e98316c.mock.pstmn.io";

    private static final String Get_Success = "/ownerName/success";
    private static final String Get_UnSuccess = "/ownerName/unsuccess";
    private static final String POST_createSomething_OK = "/createSomething?permission=yes";
    private static final String POST_createSomething = "/createSomething";
    private static final String PUT_updateMe = "/updateMe";
    private static final String DELETE_deleteWorld = "/deleteWorld";

    private static final String SessionID = "123456789";

    private static final String Content_Type = "application/json";

    @BeforeClass
    public void setUp(){
        RestAssured.baseURI = baseUrl;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

    @Test()
    public void GetAction(){
        Response response = given()
                .get(Get_Success);
        response.then()
                .statusCode(HttpStatus.SC_OK);

        System.out.println("\nТест 1. Get Success Відповідь серверу: status code = " + response.statusCode() + " Name = " + response.jsonPath().get("name"));
    }
    @Test(dependsOnMethods = "GetAction")
    public void GetActionBed(){
        Response response = given()
                .get(Get_UnSuccess);
        response.then()
                .statusCode(HttpStatus.SC_FORBIDDEN);

        System.out.println("\nТест 2. Get UnSuccess Відповідь серверу: status code = " + response.statusCode() + " Exception = " + response.jsonPath().get("exception"));
    }

    @Test(dependsOnMethods = "GetActionBed")
    public void PostAction_200_OK(){

        Response response = given()
                .post(POST_createSomething_OK);
        response.then()
                .statusCode(HttpStatus.SC_OK);
        System.out.println("\nТест 3. POST createSomething_200_OK Відповідь серверу: status code = " + response.statusCode() + " Rezult = " + response.jsonPath().get("rezult"));
    }

    @Test(dependsOnMethods = "PostAction_200_OK")
    public void PostAction_400(){

        Response response = given()
                .post(POST_createSomething);
        response.then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
        System.out.println("\nТест 4. POST createSomething 400 Відповідь серверу: status code = " + response.statusCode() + " Rezult = " + response.jsonPath().get("rezult"));
    }

    @Test(dependsOnMethods = "PostAction_400")//hzzzzzzzzzzzzzz
    public void PutAction(){

        Map<String, ?> body = Map.of(
                "name", "Viktor",
                "surname", "Kirianov"
        );

        Response response =  given().body(body)
                .header("Content-Type", Content_Type)
                .post(PUT_updateMe);
        response.then()
               .statusCode(HttpStatus.SC_BAD_GATEWAY);

        System.out.println("\nТест 5. PUT_updateMe Відповідь серверу: status code = " + response.statusCode());
    }

    @Test(dependsOnMethods = "PutAction")
    public void DeleteAction(){
        Response response = given()
                .header("SessionID", SessionID)
                .delete(DELETE_deleteWorld);
        response.then()
                .statusCode(HttpStatus.SC_GONE);

        System.out.println("\nТест 6. DELETE_deleteWorld Відповідь серверу: status code = " + response.statusCode() + " World = " + response.jsonPath().get("world"));
    }
}
