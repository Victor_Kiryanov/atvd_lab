package org.example.lab2;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class Individual {

    private static final String baseUrl = "https://petstore.swagger.io/v2";

    private static final String PET = "/pet",
            PET_PETID = PET + "/{petId}",

            PET_FINDBUSTATUS = PET + "/findByStatus";
    private static final String apiKey = "special-key";

    private String petId;
    private String petName;

    private String tagName = "KirianovViktor";

    void responseTest(Response response){
        System.out.println("Пес з ім'ям " + response.jsonPath().get("name").toString() + " індентифікатор пса: " + response.jsonPath().get("id").toString());
        System.out.println("Категорія " + response.jsonPath().get("category.name").toString());
        System.out.println("Тег " + response.jsonPath().get("tags.name").toString() + " Статус " + response.jsonPath().get("status").toString());
    }
    @BeforeClass
    public void setUp(){
        RestAssured.baseURI = baseUrl;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

   @Test
    public void verifyAddNewPet(){

       String requestBody = " {\n" +
               "  \"category\": {\n" +
               "    \"id\": 0,\n" +
               "    \"name\": \"122-20ск-1\"\n" +
               "  },\n" +
               "  \"name\": \"" + Faker.instance().dog().name() + "\",\n" +
               "  \"photoUrls\": [\n" +
               "    \"" + Faker.instance().dog().memePhrase() + "\"\n" +
               "  ],\n" +
               "  \"tags\": [\n" +
               "    {\n" +
               "      \"id\": 0,\n" +
               "      \"name\": \"" + tagName + "\"\n" +
               "    }\n" +
               "  ],\n" +
               "  \"status\": \"available\"\n" +
               "}";

      Response response = given().contentType(ContentType.JSON).body(requestBody)
               .post(PET);
      response.then()
               .statusCode(HttpStatus.SC_OK);

       petId = response.jsonPath().get("id").toString();

       System.out.println("Тест 1. Відповідь серверу стосовно інформації про додану тварину: ");
       responseTest(response);
   }

    @Test(dependsOnMethods = "verifyAddNewPet")
    public void verifyGetPet(){
        Response response = given().pathParams("petId", petId)
                .get(PET_PETID);
        response.then()
                .statusCode(HttpStatus.SC_OK);

        petName = response.jsonPath().get("name").toString();

        System.out.println("\nТест 2. Відповідь серверу стосовно пошуку тварини за індентифікатором: ");
        responseTest(response);
    }

    @Test(dependsOnMethods = "verifyAddNewPet", priority = 1)
    public void verifyFindByStatus(){

        Map<String, ?> body = Map.of(
                "status", "available"
        );

        Response response = given().body(body)
                .get(PET_FINDBUSTATUS);
        response.then()
                .statusCode(HttpStatus.SC_OK);

        System.out.println("\nТест 3. Отримати усих твари зі статусом available. Статус виконання запиту = " + response.statusCode());
    }

    @Test(dependsOnMethods = "verifyGetPet", priority = 3)
    public void verifyPutPet(){

        String requestBody = "{\n" +
                "  \"id\":"+ petId +",\n" +
                "  \"category\": {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"122-20ск-1.15\"\n" +
                "  },\n" +
                "  \"name\": \"" + petName +"\",\n" +
                "  \"photoUrls\": [\n" +
                "    \"string\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": 0,\n" +
                "      \"name\": \"" + tagName + "\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \"pending\"\n" +
                "}";

        Response response = given().contentType(ContentType.JSON).body(requestBody)
                .put(PET);
        response.then()
                .statusCode(HttpStatus.SC_OK);

        System.out.println("\nТест 4. Оновлення даних про пса: ");
        responseTest(response);
    }

    @Test(dependsOnMethods = "verifyPutPet")
    public void verifyDeletePet(){

        Response response = given().pathParams("petId", petId).header("api_key", apiKey)
                .delete(PET_PETID);
        response.then()
                .statusCode(HttpStatus.SC_OK);

        System.out.println("\nТест 5. Видалення пса. Статус від серверу " + + response.statusCode());
    }
}